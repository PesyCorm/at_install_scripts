#!/bin/sh
python3 -m venv env
echo "env installed"
source ./env/bin/activate || echo "Ошибка при активации виртуального окружения" && return
echo "env activated"
pip3 install -r requirements.txt
echo "tests packages installed"
cd ./env/lib/python3*/site-packages || echo "env dir not found" && return
git clone https://gitlab.com/PesyCorm/qa_tools.git
echo "qa_tools cloned"
cd ./qa_tools
pip3 install -r requirements.txt
echo "qa_tools packages installed"
pip3 install git+https://git.zonatelecom.ru/qa/quack.git
echo "quack installed"