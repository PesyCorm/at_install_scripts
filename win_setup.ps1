python -m venv env
Write-Host "env installed"
.\env\Scripts\activate
Write-Host "env activated"
pip install -r requirements.txt
Write-Host "tests packages installed"
cd .\env\Lib\site-packages
git clone https://gitlab.com/PesyCorm/qa_tools.git
Write-Host "qa_tools cloned"
cd .\qa_tools
pip install -r requirements.txt
Write-Host "qa_tools packages installed"
pip3 install git+https://git.zonatelecom.ru/qa/quack.git
Write-Host "quack installed"